/// <reference types="react" />

import { default as cloneDeep } from 'lodash.clonedeep';
import { ComponentType } from 'react';
import { Context } from 'react';
import { default as debounce } from 'lodash.debounce';
import { ExperimentConfig } from '@hnbi/experiments/jsx-runtime';
import { FunctionComponent } from 'react';
import { ImgHTMLAttributes } from 'react';
import { NavigateFn } from '@reach/router';
import * as React_2 from 'react';
import { WindowLocation } from '@reach/router';

export declare const addBasketToUV: (line_items: ATGBasketItem[] | SessionBasketItem[], isATG?: boolean) => void;

export declare const addSessionToUV: ({ userId, loginStatus, ochId, name, email, digital_identity_id, }: {
    userId?: string | undefined;
    loginStatus: boolean;
    ochId?: string | undefined;
    name?: string | undefined;
    email?: string | undefined;
    digital_identity_id?: string | undefined;
}) => Promise<void>;

export declare const addTrailingSlash: (value: string, host?: string) => string;

export declare type AnalyticsDataNode = MagnoliaNode<{
    field?: MagnoliaBoolean;
    action?: string;
    category?: string;
    label?: string;
}>;

export declare interface AnalyticsEventOptions {
    event_category?: string;
    event_label?: string;
    event_value?: number;
    non_interaction?: boolean;
    [customDimension: string]: boolean | number | string | undefined;
}

/**
 * Provides the necessary context for components to function.
 * This provider is composed of several other providers.
 */
export declare const AppProvider: ({ children, basketType, geoOverlayEnabled, site, ssUrl, sessionTimeout, featureFlags, }: AppProviderProps) => JSX.Element;

export declare interface AppProviderProps {
    children: React.ReactNode;
    basketType?: BasketType;
    geoOverlayEnabled?: boolean;
    /**
     * Object holding site information for the current page. A site object can be crafted by hand or derived from the page properties of a magnolia page with
     * a site template, e.g. https://magnolia-public.eu-west-1.prod.hbi.systems/.magnolia/admincentral#app:pages-app:detail;/site/uk/en-fr:edit.
     * Content for a site object can be fetched either individually or as a whole. To fetch data for a single site you can specify the path
     * of the site page in the URL of the request - https://magnolia-public.eu-west-1.prod.hbi.systems/.rest/delivery/v1/sites/site/uk/en-fr/.
     * Or you can fetch all sites at once with https://magnolia-public.eu-west-1.prod.hbi.systems/.rest/delivery/v1/sites/?mgnl:template=attract:pages/Site.
     * To create a site object from the fetched content to pass here, you can use the `createSiteFromNode` (or `createSitesFromNodes`) utility.
     */
    site?: Partial<MagnoliaSite>;
    ssUrl?: string;
    sessionTimeout?: number;
    featureFlags?: Record<string, boolean>;
}

export declare interface ATGBasketItem {
    every: number;
    everyPeriod: number;
    itemType: number;
    productInfo: {
        productId: string;
        skuId: string;
    };
    promoAttractMessage: string;
    promoNames: string[];
    quantity: number;
    subtotal: number;
    total: number;
}

export declare interface AtgBasketItem {
    id: string;
    skuId: string;
    quantity: number;
}

export declare interface AtgSession {
    success?: boolean;
    loaded: boolean;
    basketTotal: number;
    basketItems?: BasketItem[];
    currency: Currency;
    favouriteItems: any[];
    rewardsCards: any[];
    userId: string;
    name?: string;
    email?: string;
    csrAgentID?: string;
    loginStatus: boolean;
    /**
     * @deprecated
     */
    dynSessConfToken: string;
    rflpoints?: number;
    coupons?: {
        code: string;
    }[];
    ochId?: string;
    isAuth0Enabled: boolean;
}

export declare interface Auth0Session {
    success?: boolean;
    loaded: boolean;
    basketTotal: number;
    basketItems?: BasketItem[];
    currency: Currency;
    rewardsCards: any[];
    name?: string;
    email?: string;
    loginStatus: boolean;
    rflpoints?: number;
    coupons?: {
        code: string;
    }[];
    ochId?: string;
    isAuth0Enabled: boolean;
    userId: string;
    /**
     * @deprecated
     */
    csrAgentID?: string;
    /**
     * @deprecated
     */
    favouriteItems: any[];
    /**
     * @deprecated
     */
    dynSessConfToken?: string;
}

export declare interface Auth0SessionResponse {
    loginStatus: boolean;
    digital_identity_id?: string;
    email?: string;
    name?: string;
    firstName?: string;
    lastName?: string;
    och_id?: string;
    atg_id?: string;
}

export declare interface BasketConfig {
    currency?: Currency;
    locale?: string;
    siteId?: number;
    addButtonLabel?: string;
}

export declare type BasketItem = AtgBasketItem & HorizonBasketItem;

export declare enum BasketType {
    ATG = "ATG",
    Horizon = "Horizon"
}

export declare const BREAKPOINT: {
    readonly mobile: 768;
    readonly tablet: 1040;
    readonly desktop: number;
};

export declare const BreakpointContext: Context<number | undefined>;

/**
 * Provides screen breakpoint information. Breakpoint is `undefined` during SSR.
 * @deprecated
 */
export declare const BreakpointProvider: ({ children }: BreakpointProviderProps) => JSX.Element;

export declare interface BreakpointProviderProps {
    children: React.ReactNode;
}

export declare const capitalize: (value: string) => string;

export declare type CdnResolver = ({ bucketName, path, }: {
    bucketName: string;
    path: string;
}) => string;

export declare class Channel {
    #private;
    constructor(namespace: string);
    destroy(): void;
    emit(event: string, data?: any): void;
    on(event: string, callback: (data?: any) => void): void;
    off(event: string, callback: (data?: any) => void): void;
    once(event: string, callback: (data?: any) => void): void;
}

export { cloneDeep }

export declare const createBreadcrumbs: (categoryNode: HealthHubCategoryNode, basePath: string, healthHubCategoryNodes: HealthHubCategoryNode[], pagePathPrefix: string) => {
    name: string;
    path: string;
}[];

export declare const createCategoryPath: (categoryNodePath: string, basePath: string, pagePathPrefix?: string) => string;

/**
 * Wrapper to create a magnolia config for an EditablePage component. Adds the ability
 * for components rendered by @magnolia/react-editor to be overriden by experiments and
 * also wraps each component in an error boundary.
 */
export declare const createMagnoliaConfig: (componentMappings: {
    [template: string]: ComponentType<any>;
}) => {
    componentMappings: {
        [k: string]: ComponentType<any>;
    };
};

export declare const createSiteFromNode: <T>(siteNode: MagnoliaNode<T, any>) => MagnoliaSite;

/**
 * Creates site objects based on site page content from magnolia.
 */
export declare const createSitesFromNodes: <T>(siteNodes: MagnoliaNode<T, any>[], categoryBasePath?: string) => MagnoliaSite[];

export declare enum Currency {
    GBP = "GBP",
    EUR = "EUR"
}

export { debounce }

export declare const detectBrowser: () => "Edge" | "Internet Explorer" | "Firefox" | "Opera" | "Chrome" | "Safari" | "Other";

export declare const detectDeviceType: () => "Tablet" | "Mobile" | "Desktop";

export declare const detectUserOS: () => UserOS | undefined;

export declare const experiments: ExperimentConfig[];

export declare const extractCategoriesFromPath: (categoryNode: HealthHubCategoryNode, healthHubCategoryNodes: HealthHubCategoryNode[]) => HealthHubCategoryNode[];

export declare const extractHealthHubArticleDataFromTopBar: (node: HealthHubArticleNode, dataKey: 'image1' | 'title') => string;

declare type FallbackProps = {
    src: string;
} & Partial<IResponsiveImageProps>;

export declare interface FavouriteItem {
    favouriteId: string;
    digitalIdentityId: string;
    skuId: string;
    productId: string;
    quantity: number;
    datetimeCreated: string;
    datetimeUpdated: string;
}

export declare interface FeatureFlags {
    flags: Record<string, boolean>;
    site: MagnoliaSite;
}

export declare const formatCamelCase: (entry: string) => string;

export declare const formatCurrency: (value: string | number, currency?: Currency) => string;

export declare const formatPhoneNumber: (entry: string) => string;

export declare const formatStoreNameAndCapitalize: (storeName?: string) => string | undefined;

export declare interface GatsbyImageFileNode {
    childImageSharp?: {
        gatsbyImageData: IGatsbyImageData;
    };
    publicURL?: string;
    url?: string;
}

export declare interface GatsbyPageContext<P = PageNode> {
    brandCategories?: {
        [category: string]: {
            label: string;
            slug: string;
        }[];
    };
    healthHubArticlePathMap?: {
        [nodePath: string]: string;
    };
    healthHubAuthors?: HealthHubAuthorNode[];
    healthHubCategories?: HealthHubCategory[];
    healthHubLatestArticles?: HealthHubArticleNode[];
    healthHubRootPage?: {
        localePath: MagnoliaLocalePath;
        pageNode: HealthHubRootNode;
    };
    healthHubVisibleInCategories?: HealthHubCategory[];
    id?: string;
    images?: GatsbyImageFileNode[];
    isGeoOverlayEnabled?: boolean;
    isHorizonBasketEnabled?: boolean;
    isIeHorizonBasketEnabled?: boolean;
    isNlHorizonBasketEnabled?: boolean;
    isUxExperimentsEnabled?: boolean;
    locale?: MagnoliaLocale;
    localePath?: MagnoliaLocalePath;
    metadataOverride?: SeoMetadataOverride;
    pageNode?: P;
    products?: OnesearchProduct[];
    site?: Omit<MagnoliaSite, 'host' | 'hrefLang' | 'parentSite'>;
    storeDetails?: StoreDetail[];
    relatedContent?: RelatedContent[];
    template?: string;
    optimizelySdkKey?: string;
    featureFlags?: Record<string, boolean>;
}

export declare const genElementId: (prefix: string, name: string | undefined) => string;

export declare const getEmptyStyleElement: (styleElementId: string) => HTMLStyleElement;

export declare const getRootCategoryNodeByPage: (nodePath: string, healthHubCategoryNodes: HealthHubCategoryNode[]) => HealthHubCategoryNode | undefined;

export declare const getSiteByBasePath: (basePath: string, sites: MagnoliaSite[]) => MagnoliaSite | undefined;

export declare const getSiteByNodePath: (nodePath: string, sites: MagnoliaSite[]) => MagnoliaSite | undefined;

export declare const getSitespectEngineApiUrl: () => string;

export declare const getStoredItem: <T = any>(key: string, storage?: Storage | undefined) => T | undefined;

export declare const getViewport: <T extends {
    [viewport: string]: number;
} = {
    readonly mobile: 768;
    readonly tablet: 1040;
    readonly desktop: number;
}, K extends keyof T = keyof T>(breakpoint?: T | undefined) => K | undefined;

export declare const hasBlacklistedWords: (values: any[] | {
    [key: string]: any;
}, blacklist?: string[]) => boolean;

export declare type HealthHubArticleNode = MagnoliaNode<{
    authors?: (HealthHubAuthorNode | string)[];
    content?: MagnoliaNode;
    shortTitle?: string;
    description?: string;
    topBar?: {
        '0': {
            image1?: string;
            title?: string;
        };
        '@nodes'?: string[];
    };
    wpCreationDate?: string;
    datePublished?: string;
    excludePage?: MagnoliaBoolean;
    showCarousel?: MagnoliaBoolean;
    carouselTitle?: string;
}>;

export declare type HealthHubAuthorNode = MagnoliaNode<{
    title?: string;
    firstname: string;
    surname: string;
    jobtitle?: string;
    image?: string;
    joined?: string;
    joinedTitle: string;
    qualifications?: string;
    about?: string;
    email?: string;
    aboutTitle: string;
    expertiseTitle: string;
    testimonialsTitle: string;
    expertise?: MagnoliaNode;
    testimonials?: MagnoliaNode;
    links?: MagnoliaNode;
}>;

export declare interface HealthHubCategory {
    name: string;
    path: string;
}

export declare type HealthHubCategoryNode = MagnoliaNode<{
    displayName?: string;
    name?: string;
    level?: string;
    menuColor?: string;
    description?: string;
    orderInParentMenu?: string;
    pageTitle?: string;
    subTitle?: string;
    topArticles?: string[];
    visibleAsButtonInParentMenu?: MagnoliaBoolean;
}>;

export declare type HealthHubRecipeNode = MagnoliaNode<{
    topBar?: {
        '0': {
            image1?: string;
            title?: string;
        };
        '@nodes'?: string[];
    };
    content?: MagnoliaNode;
    authors?: (HealthHubAuthorNode | string)[];
    description?: string;
    introduction?: string;
    datePublished?: string;
    dateText?: string;
    excludePage?: MagnoliaBoolean;
    cookTime?: string;
    diets: MagnoliaNode<{
        dietText?: string;
        diet: 'glutenFreeDiet' | 'lowLactoseDiet' | 'fodmapDiet' | 'lowSugarDiet' | 'lowCarbDiet' | 'ketoDiet' | 'veganDiet' | 'vegetarianDiet';
    }>;
    difficulty?: 'easy' | 'medium' | 'hard';
    image1?: string;
    image2?: string;
    image3?: string;
    noIndex?: MagnoliaBoolean;
    nutritionCalories?: string;
    nutritionCarbs?: string;
    nutritionFat?: string;
    nutritionFibre?: string;
    nutritionProtein?: string;
    nutritionSaturates?: string;
    nutritionSugar?: string;
    nutritionSalt?: string;
    prepTime?: string;
    recipeCategory?: string;
    recipeCuisine?: string;
    recipeIngredient: MagnoliaNode<{
        ingredient: string;
    }>;
    recipeInstructions: MagnoliaNode<{
        step: string;
    }>;
    recipeYield?: string;
    recipeYieldContext?: string;
    shortTitle: string;
    title: string;
    videoId?: string;
    videoSource?: string;
    difficultyText?: string;
}>;

export declare type HealthHubRootNode = MagnoliaNode<{
    shortTitle?: string;
    subTitle?: string;
    menu?: MagnoliaNode;
    content?: MagnoliaNode;
    truncatedButtonText?: string;
    expandedButtonText?: string;
    joinedTitle?: string;
    aboutTitle?: string;
    expertiseTitle?: string;
    testimonialsTitle?: string;
    readMoreAuthorLink?: string;
    relatedTopicsText?: string;
    suggestedArticlesText?: string;
    authorShortTitle?: string;
    authorLabel?: string;
    ubCampaignId?: string;
    aboutLabel?: string;
    minText?: string;
    hourText?: string;
    byText?: string;
    publishedText?: string;
    servesText?: string;
    prepText?: string;
    cookText?: string;
    totalText?: string;
    nutritionText?: string;
    caloriesText?: string;
    proteinText?: string;
    fatText?: string;
    saturatesText?: string;
    fibreText?: string;
    carbsText?: string;
    sugarText?: string;
    saltText?: string;
    ingredientsText?: string;
    methodText?: string;
}, 'menu' | 'content'>;

export declare interface HodParams {
    on?: Array<'visible' | 'idle' | 'delay' | ['delay', number] | ['scroll', () => Element]>;
}

export declare interface HodProps {
    forceHydration?: boolean;
    wrapperProps?: React.HTMLAttributes<HTMLDivElement>;
}

export declare const HORIZON_BASKET_SESSION_RESPONSE_MOCK: {
    session: {
        dynSessConfToken: string;
        success?: boolean | undefined;
        loaded: boolean;
        basketTotal: number;
        basketItems?: BasketItem[] | undefined;
        currency: Currency;
        favouriteItems: any[];
        rewardsCards: any[];
        userId: string;
        name?: string | undefined;
        email?: string | undefined;
        csrAgentID?: string | undefined;
        loginStatus: boolean;
        rflpoints?: number | undefined;
        coupons?: ({
            code: string;
        }[] & {
            code: string;
        }[]) | undefined;
        ochId?: string | undefined;
        isAuth0Enabled: boolean;
    };
};

export declare interface HorizonBasketItem {
    id: string;
    quantity: number;
    productId: string;
    name: string;
    total: number;
    promoNames: string[];
    available: boolean;
    images: {
        path: string;
    }[];
}

declare interface IGatsbyImageData {
    layout: Layout;
    width: number;
    height: number;
    backgroundColor?: string;
    images: Pick<MainImageProps, "sources" | "fallback">;
    placeholder?: Pick<PlaceholderProps, "sources" | "fallback">;
}

export declare const INITIAL_SESSION: Session;

declare interface IResponsiveImageProps {
    sizes?: string;
    srcSet: string;
}

export declare const isMagnoliaNode: <T>(node: any) => node is MagnoliaNode<T, any>;

export declare const isPromise: <T>(promise: any) => promise is Promise<T>;

declare type Layout = "fixed" | "fullWidth" | "constrained";

export declare interface LineItem {
    product: {
        id: string;
        sku_code: string;
        name: string;
        thumbnail_url: string;
        unit_sale_price: string;
        unit_price: string;
        url: string;
        category_name: string;
        subcategory_name: string;
        promotions: string | null;
    };
    quantity: number;
}

export declare interface ListingItem {
    id: string;
    name: string;
    sku_code: string;
    unit_pricing: string;
    unit_sale_price: string;
    product_impression_listing: string;
    product_impression_position: number;
    category_name: string;
}

export declare const loadScript: (scriptId: string, src: string, attributes?: {
    [key: string]: string;
} | undefined) => Promise<void>;

export declare const LocationContext: Context<    {
country_code: string;
}>;

export declare interface LocationData {
    currentLocation?: string;
    isLocation?: boolean;
    specificLocationMessage?: string;
    continent?: string;
    country_code?: string;
    country_name?: string;
}

export declare const LocationProvider: ({ children, }: {
    children: React.ReactNode;
}) => JSX.Element;

export declare enum LoginStatus {
    IN = "loggedin",
    OUT = "guest"
}

export declare const logout: (triggerUpdate: () => void, isAuth0Enabled?: boolean) => Promise<void>;

export declare type MagnoliaBoolean = 'true' | 'false';

export declare const magnoliaConfig: {
    componentMappings: {
        [k: string]: ComponentType<any>;
    };
};

export declare interface MagnoliaFlag {
    backgroundColor?: string;
    color?: string;
    name?: string;
    skus?: string[];
    text?: string;
}

export declare interface MagnoliaLocale {
    localePaths: MagnoliaLocalePath[];
    mainPath: string;
}

export declare interface MagnoliaLocalePath {
    /**
     * Subsite path for the page, e.g. /en-us or /fr. This should be `undefined` if the page is not on a subsite.
     */
    baseSubsitePath?: string;
    /**
     * Domain the page resides on, e.g. hollandandbarrett.com
     */
    host?: string;
    /**
     * Language code (ISO 639-1) used for href lang, e.g. nl
     * https://www.w3.org/International/questions/qa-html-language-declarations#attributes
     */
    htmlLang?: string;
    /**
     * Language code (ISO 639-1) used for href lang, e.g. en-GB
     */
    hrefLang?: string;
    /**
     * True if the page is on the main site (hollandandbarrett.com)
     */
    isMainPath?: boolean;
    /**
     * The path of the page on the main site. This will be the same as the page path if
     * the page is on the main site or if there is no equivalent page on the main site.
     */
    mainPath?: string;
    /**
     * Path of the page, e.g. /shop/vitamins-supplements/
     */
    pagePath: string;
}

export declare interface MagnoliaMetadata<T = string> {
    '@name': string;
    '@id': string;
    '@nodeType': string;
    '@nodes': T[];
    '@path': string;
    'mgnl:template'?: string;
    'mgnl:lastModified'?: string;
    jcrName?: string;
}

export declare type MagnoliaNode<T = any, K = any> = MagnoliaMetadata<K> & {
    title?: string;
    shortTitle?: string;
} & {
    [P in keyof T]: T[P];
};

export declare interface MagnoliaRuleSet extends MagnoliaMetadata {
    voters: MagnoliaMetadata & MagnoliaNode;
}

export declare interface MagnoliaSite {
    basePath: string;
    /**
     * Subsite path for the page, e.g. /en-us or /fr. This should be `undefined` if the page is not on a subsite.
     */
    baseSubsitePath?: string;
    basket?: BasketConfig;
    globalBannersNode?: MagnoliaNode;
    globalLayoutNode?: MagnoliaNode;
    globalPopupsNode?: MagnoliaNode;
    host?: string;
    htmlLang?: string;
    hrefLang?: string;
    name: string;
    onesearch?: {
        locale?: string;
        siteId?: number;
    };
    unitsOfMeasurement?: 'metric' | 'imperial';
    parentSite?: MagnoliaSite;
    robots?: string;
    pagePathPrefix?: string;
    categoryBasePath?: string;
    subsites?: MagnoliaSite[];
    seoPaths?: string[];
    enableRelatedContent?: boolean;
    rflPointsRule?: '2' | '4';
    optimizelySdkKey?: string;
    gtmContainerId?: string;
}

export declare interface MagnoliaVariant extends MagnoliaMetadata {
    'mgnl:ruleSet'?: MagnoliaRuleSet;
}

export declare interface MagnoliaYamlAvailableComponents {
    [key: string]: {
        id: string;
    };
}

export declare interface MagnoliaYamlConfig {
    title: string;
    dialog: string;
    areas?: {
        [key: string]: {
            availableComponents: MagnoliaYamlAvailableComponents;
        };
    };
}

declare type MainImageProps = PictureProps;

export declare const mapHealthHubCategoryNode: (categoryNode: HealthHubCategoryNode, basePath: string, pagePathPrefix: string) => {
    name: string;
    path: string;
};

export declare const mapMagnoliaNodes: <T, U = T[any], V = U>(node?: Partial<MagnoliaNode<T, any>> | U[] | undefined, callback?: ((node: U, index: number) => V) | undefined) => V[];

export declare interface OnesearchBrand {
    key: string;
    doc_count: number;
    label: string;
}

export declare interface OnesearchProduct {
    id: string;
    name: string;
    images: {
        group: string;
        url: string;
    }[];
    final_price: number;
    list_price: number;
    not_available: boolean;
    sale_price?: string;
    product: {
        name: string;
        id: string;
    };
    promotions: {
        text: string;
    }[];
    quantity: {
        unit: string;
        has_multiplier: boolean;
        value: number;
    };
    rating: number;
    review_count: number;
    summary: string;
    flags?: MagnoliaFlag[];
}

export declare type OnesearchProductResult = Omit<OnesearchProduct, 'images'> & {
    images?: {
        list: {
            path: string;
            resolution: number;
        }[];
        group: string;
    }[];
};

export declare interface OnesearchRelatedContent {
    id: string;
    related_links: {
        title: string;
        description: string;
        url: string;
        image: string;
    }[];
}

export declare interface OnesearchSeoMetadataOverride {
    url: string;
    title?: string;
    description?: string;
}

export declare interface OnesearchSuggestion {
    type: 'content' | 'cta' | 'heading' | 'product' | 'term' | 'articles' | 'search' | 'brand';
    id?: string;
    text?: string;
    href?: string;
    thumbnail?: {
        src: string;
        alt: string;
    };
    clear?: boolean;
}

export declare interface OnesearchSuggestionResult {
    data: {
        id: string;
        name: string;
        is_valid?: boolean;
        no_match?: boolean;
    };
    images: {
        path: string;
    }[];
    slug: string;
}

export declare const originCdnResolver: CdnResolver;

export declare type Page<PageContext extends GatsbyPageContext = GatsbyPageContext, LocationState = {}> = PageProps<{}, PageContext, LocationState>;

export declare const PageContext: Context<Partial<Page<GatsbyPageContext<PageNode>, {}>>>;

export declare type PageNode = MagnoliaNode<{
    breadcrumbCategories?: HealthHubCategoryNode[];
    isBackgroundDark?: MagnoliaBoolean;
    mainPath?: MagnoliaNode;
    primaryCategory?: HealthHubCategoryNode;
    visibleInCategories?: HealthHubCategoryNode[];
    noIndex?: MagnoliaBoolean;
    description?: string;
}>;

/**
 * A props object for adding type safety to your Gatsby pages, can be
 * extended with both the query response shape, and the page context.
 *
 * @example
 * // When typing a default page from the ./pages dir
 *
 * import {PageProps} from "gatsby"
 * export default (props: PageProps) => {
 *
 * @example
 * // When adding types for both pageContext (represented by LocaleLookUpInfo)
 * // and GraphQL query data (represented by IndexQueryProps)
 *
 * import {PageProps} from "gatsby"
 *
 * type IndexQueryProps = { downloadCount: number }
 * type LocaleLookUpInfo = { translationStrings: any } & { langKey: string, slug: string }
 * type IndexPageProps = PageProps<IndexQueryProps, LocaleLookUpInfo>
 *
 * export default (props: IndexPageProps) => {
 *   ..
 */
declare type PageProps<
DataType = object,
PageContextType = object,
LocationState = WindowLocation["state"],
ServerDataType = object
> = {
    /** The path for this current page */
    path: string
    /** The URI for the current page */
    uri: string
    /** An extended version of window.document which comes from @react/router */
    location: WindowLocation<LocationState>
    /** A way to handle programmatically controlling navigation */
    navigate: NavigateFn
    /** You can't get passed children as this is the root user-land component */
    children: undefined
    /** The URL parameters when the page has a `matchPath` */
    params: Record<string, string>
    /** Holds information about the build process for this component */
    pageResources: {
        component: React_2.Component
        json: {
            data: DataType
            pageContext: PageContextType
        }
        page: {
            componentChunkName: string
            path: string
            webpackCompilationHash: string
            matchPath?: string
        }
    }
    /**
     * Data passed into the page via an exported GraphQL query. To set up this type
     * you need to use [generics](https://www.typescriptlang.org/play/#example/generic-functions),
     * see below for an example
     *
     * @example
     *
     * import {PageProps} from "gatsby"
     *
     * type IndexQueryProps = { downloadCount: number }
     * type IndexPageProps = PageProps<IndexQueryProps>
     *
     * export default (props: IndexPageProps) => {
     *   ..
     *
     */
    data: DataType
    /**
     * A context object which is passed in during the creation of the page. Can be extended if you are using
     * `createPage` yourself using generics:
     *
     * @example
     *
     * import {PageProps} from "gatsby"
     *
     * type IndexQueryProps = { downloadCount: number }
     * type LocaleLookUpInfo = { translationStrings: any } & { langKey: string, slug: string }
     * type IndexPageProps = PageProps<IndexQueryProps, LocaleLookUpInfo>
     *
     * export default (props: IndexPageProps) => {
     *   ..
     */
    pageContext: PageContextType
    /** Data passed into the page via the [getServerData](https://www.gatsbyjs.com/docs/reference/rendering-options/server-side-rendering/) SSR function. */
    serverData: ServerDataType
}

/**
 * Holds Gatsby [`pageContext`](https://www.gatsbyjs.com/docs/creating-and-modifying-pages/#pass-context-to-pages).
 * Context is accessible through the `usePage` hook.
 */
export declare const PageProvider: ({ children, page, site }: PageProviderProps) => JSX.Element;

export declare interface PageProviderProps {
    children: React.ReactNode;
    page?: Partial<Page>;
    site?: Partial<MagnoliaSite>;
}

export declare const parseBoolean: (value?: boolean | MagnoliaBoolean) => boolean;

declare type PictureProps = ImgHTMLAttributes<HTMLImageElement> & {
    fallback?: FallbackProps;
    sources?: Array<SourceProps>;
    alt: string;
    shouldLoad?: boolean;
};

declare type PlaceholderProps = ImgHTMLAttributes<HTMLImageElement> & {
    fallback?: string;
    sources?: Array<SourceProps>;
};

export declare interface RelatedContent {
    title: string;
    description: string;
    url: string;
    image: string;
}

export declare const removeStoredItem: (key: string, storage?: Storage | undefined) => void;

/**
 * converts s3 dam ids (e.g. s3:hbi-legacy-atg-images-prod/logos/hblogo-primary.svg) from magnolia into normal image urls.
 * by default, all images will resolve to images.hollandandbarrett.co.uk (https://images.hollandandbarrettimages.co.uk/logos/hblogo-primary.svg)
 *
 * @param magnoliaNode any magnolia node
 * @param cdnResolver function to resolve the CDN url for a given image
 * @returns list of images that were resolved
 */
export declare const resolveMagnoliaImages: <T>(magnoliaNode?: MagnoliaNode<T, any> | MagnoliaNode<T, any>[] | undefined, cdnResolver?: CdnResolver) => string[];

export declare const rewriteUrl: (input: string | RewriteUrlOptions) => string | undefined;

export declare interface RewriteUrlOptions {
    host?: string;
    href?: string;
    isAuth0Enabled?: boolean;
    pathPrefix?: string;
}

export declare interface RflCard {
    createdDate: Date | string;
    cardNumber: string;
}

export declare interface RflCoupon {
    number: string;
    status: string;
    amount: number;
    start: string;
    expiry: string;
    used: string | null;
    currency: string;
}

export declare interface RflCouponData {
    available: RflCoupon[];
    expired: RflCoupon[];
}

export declare interface RflData {
    points: number;
    cards: RflCard[];
    vouchers: RflCouponData | RflCoupon[];
}

/** response data from /api/rfl */
export declare interface RflResponse {
    rfl?: {
        data: RflData;
        pointsLoading: boolean;
        pointsErrored: boolean;
        vouchersLoading: boolean;
        vouchersErrored: boolean;
        cardsLoading: boolean;
        cardsErrored: boolean;
    };
    errorMessage?: string;
}

/**
 * Stores scripts to be later added in the final HTML output.
 */
export declare class ScriptRegistry {
    #private;
    add(bundle?: string): void;
    /**
     * Obtain all scripts that have been collected as an array of script elements.
     */
    getScriptElements(): JSX.Element[];
    /**
     * Obtain all scripts that have been collected as an array of html strings.
     */
    getScriptTags(): string[];
}

export declare const ScriptRegistryContext: Context<ScriptRegistry | undefined>;

/**
 * Collects all scripts required for the current page in the given registry.
 */
export declare const ScriptRegistryProvider: ({ children, registry, }: ScriptRegistryProviderProps) => JSX.Element;

export declare interface ScriptRegistryProviderProps {
    children?: React.ReactNode;
    registry: ScriptRegistry;
}

/**
 * simple non-cryptographic hash function
 * http://www.cse.yorku.ca/~oz/hash.html#sdbm
 */
export declare const sdbm: (text: string) => number;

export declare interface SearchSuggestionsText {
    categorySuggestionsHeading?: string;
    brandSuggestionsHeading?: string;
    productSuggestionsHeading?: string;
    contentSuggestionsHeading?: string;
    searchSuggestionsCTA?: string;
}

export declare const sendAnalyticsEvent: (action: string | AnalyticsDataNode | undefined, options?: AnalyticsEventOptions) => void;

export declare const sendSSConversion: (conversionId: number | string, variationIds?: string[]) => void;

export declare interface SeoMetadataOverride {
    path: string;
    title?: string;
    description?: string;
}

export declare type Session = AtgSession & Auth0Session;

export declare interface SessionBasketItem {
    skuId: string;
    quantity: number;
}

export declare const SessionContext: Context<SessionContextData>;

export declare type SessionContextData = {
    basketType: BasketType;
    data: Auth0Session | AtgSession;
    triggerUpdate: () => Promise<void>;
    addToBasket: (skuId: string, quantity?: any, incrementQuantity?: any, frequency?: any, period?: any) => Promise<void>;
};

/**
 * Fetches basket and user information on the client. Context is accessible through the `useSession` hook.
 */
export declare const SessionProvider: (props: SessionProviderProps) => JSX.Element;

export declare interface SessionProviderProps {
    basketType?: BasketType;
    timeout?: number;
    children: React.ReactNode;
}

export declare const sha256: (string: string) => Promise<string>;

export declare const shallowCompare: (a: any, b: any) => boolean;

export declare const shouldExcludeCBD: () => boolean;

export declare type SiteNode = MagnoliaNode<{
    basketCurrency?: Currency;
    basketLocale?: string;
    basketSiteId?: string;
    basketAddButtonLabel?: string;
    globalPages?: MagnoliaNode<{
        banners?: MagnoliaNode;
        layout?: MagnoliaNode;
        popups?: MagnoliaNode;
    }, 'banners' | 'layout' | 'popups'>;
    host?: string;
    htmlLang?: string;
    hrefLang?: string;
    pagePathPrefix?: string;
    onesearchLocale?: string;
    onesearchSiteId?: string;
    robots?: string;
    seoPaths?: string[];
    unitsOfMeasurement?: 'metric' | 'imperial';
    enableRelatedContent?: MagnoliaBoolean;
    rflPointsRule?: '2' | '4';
    optimizelySdkKey?: string;
    gtmContainerId?: string;
}, 'globalPages'>;

export declare type SitespectResponse = SitespectVariations & {
    cookies?: {
        SSID: string;
        SSLB: string;
        SSRT: string;
        SSSC: string;
    };
};

export declare type SitespectVariation = {
    AsmtCounted: boolean;
    Control: boolean;
    LiveVariables?: string;
    Metrics: {
        ID: number;
        Name: string;
    }[];
    Name: string;
    VariationGroup_LiveVariables?: any;
};

export declare type SitespectVariations = {
    [variationId: string]: SitespectVariation;
};

declare type SourceProps = IResponsiveImageProps & ({
    media: string;
    type?: string;
} | {
    media?: string;
    type: string;
});

export declare interface StoreDetail {
    branchNo: string;
    store: string;
    slug: string;
}

export declare const storeItem: <T>(key: string, item: T, storage?: Storage | undefined) => void;

export declare const timeoutPromise: <T>(ms: number, promise: Promise<T>, abortController?: AbortController) => Promise<T>;

export declare interface UniversalVariable {
    page: {
        type: string;
    };
    product?: {
        id: string;
        name: string;
        category_name: string;
        currency: string;
        unit_price: string;
        stock: boolean;
        sku_code: string;
    };
    listings?: {
        items: ListingItem[];
        layout: string;
        query: string;
        reslut_count: number;
        sort_by: string;
        category_name: string;
    };
    basket?: {
        line_items: LineItem[];
        orderId: string;
    };
    user: User;
    transaction?: {
        line_items: LineItem[];
        order_id: string;
        subscription: boolean;
        total: number;
        promotions: any;
    };
}

/**
 * @deprecated
 */
export declare const useBreakpoint: () => number | undefined;

export declare const useLocation: () => LocationData;

export declare const usePage: <PageContext extends GatsbyPageContext<PageNode> = GatsbyPageContext<PageNode>, LocationState = {}>() => Partial<Page<PageContext, LocationState>>;

export declare interface User {
    name?: string;
    user_id: string;
    email?: string;
    och_id?: string;
    loginstatus: LoginStatus;
}

export declare type UserOS = 'Android' | 'iOS' | 'Linux' | 'MacOS' | 'Windows' | 'Unknown OS';

export declare const useScriptRegistry: () => ScriptRegistry | undefined;

export declare const useSession: () => SessionContextData;

export declare const useVariation: (id: string, controlId?: string | null, checkFn?: () => boolean) => {
    isActive: boolean;
    isControl: boolean;
    liveVariables: any;
    name: string;
    variationId: string;
    variationLiveVariables: any;
    loading: boolean | undefined;
};

export declare const VariationContext: Context<    {
customVariations?: SitespectVariations | undefined;
activeVariations?: string[] | undefined;
variations?: SitespectVariations | undefined;
loading?: boolean | undefined;
ssUrl?: string | undefined;
countUser?: ((variationId: string) => void) | undefined;
}>;

export declare const VariationProvider: ({ children, ssUrl, variations: customVariations, }: VariationProviderProps) => JSX.Element;

export declare interface VariationProviderProps {
    children?: React.ReactNode;
    ssUrl?: string;
    variations?: SitespectVariations;
}

/**
 * perform a depth-first search of some magnolia node(s)
 *
 * @param node any magnolia node
 * @param callback callback to run on every node that has been found
 */
export declare const walkMagnoliaNodes: <T = any>(node: MagnoliaNode<T, any> | MagnoliaNode<T, any>[] | undefined, callback: (node: MagnoliaNode<T, any>, parentNode?: MagnoliaNode<T, any> | undefined) => void, parentNode?: MagnoliaNode<T, any> | undefined) => void;

export declare const withHydrationOnDemand: (params?: HodParams) => <T>(WrappedComponent: FunctionComponent<HodProps & T>) => FunctionComponent<HodProps & T>;

export declare interface WrappedComponent {
    className?: string;
}

export { }
