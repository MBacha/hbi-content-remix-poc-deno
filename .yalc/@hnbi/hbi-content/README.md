# Content / Attract

## Getting started

1. [Ensure you have the VPN set up and running](https://hbidigital.atlassian.net/wiki/spaces/PAAS/pages/842629372/VPN+Guidance).

2. Install required software:

- [Node.js v16.10 or later](https://nodejs.org/en/)

  **HINT: you can use a tool like [`nvm`](https://github.com/nvm-sh/nvm) to manage multiple versions of Node.js.**

3. Setup package manager:

```
corepack enable
```

4. [Create a new personal access token on gitlab](https://gitlab.com/profile/personal_access_tokens) with the scope `api` and add the the environment variable `CI_JOB_TOKEN` with the value of this new token. MacOS Catalina or later uses .zshrc instead of .bashrc for environment variables.

5. Clone the repository and install dependencies:

```
# Replace USERNAME with your gitlab username
git clone https://USERNAME:$CI_JOB_TOKEN@gitlab.com/HnBI/replatform/hbi-content.git
yarn install
```

6. Run the application:

```
yarn start
```

- The default site location will be `http://localhost:8000/`.
- If you do not have a local magnolia install (see below), set `GATSBY_MAGNOLIA_URL=https://magnolia-public.eu-west-1.dev.hbi.systems/.rest` before running.
- If you are low on memory, add `DISABLE_TYPE_CHECKING=true` to your `.env` file.

## Using Magnolia locally

Ensure [Magnolia is running locally on your machine](https://hbidigital.atlassian.net/wiki/spaces/ER/pages/903905281/Magnolia+-+Local+docker+install+with+custom+templates).
If you have previously run Magnolia locally, make sure you have the latest bootstrap changes by using the below commands:

In `hbi-content` repo:

```
git pull
# Adds all bootstrap files and templates to the Magnolia repository
yarn build:mgnl:develop
```

In `magnolia` repo:

```
# Stops previous containers if running
docker container stop magnolia-author magnolia-author-db
# Removes previous containers if present
docker rm magnolia-author magnolia-author-db
mvn clean prepare-package -s local_settings.xml
docker-compose up --build author author-db
```

Magnolia takes a while to build (about 15 minutes) so go make a coffee!

## Recording Tests

We use [PollyJS](https://netflix.github.io/pollyjs/#/) to record and replay HTTP requests in our test suites. To able to record tests, make sure you have imported the [test pages](https://magnolia-author.eu-west-1.dev.hbi.systems/.magnolia/admincentral#app:pages-app:browser;/test::) from the developer Magnolia instance.

Once you have the test pages in your local Magnolia instance, run the application with `GATSBY_BASE_PATH=/test`:

```
GATSBY_BASE_PATH=/test yarn start
```

Then record tests by running:

```
yarn test:record
```

> Obs: At the moment Pollyjs doesnt record tests that use the `beforeAll` function, so to have the test data recorded, all `it` blocks need to be self contained.

> Please ensure you import any new test pages you create locally back to the developer Magnolia instance test folder so that other people can edit them if needed

To record tests for storyshots, run `yarn test:storyshots-record` - the application does not need to be running for these commands. Refer to [this guide](./.storybook/README.md) for additional storybook related documentation.

## Environment Variables

Environment variables prefixed with `GATSBY_` are accessible from the browser in addition to the build process.

- `ASSET_PREFIX` - CDN for which assets will be served from, see https://www.gatsbyjs.com/docs/asset-prefix/#adding-to-gatsby-configjs
- `BUILD_ON_DATA_CHANGES` - only rebuilds the application if graphql data has changed since the previous build
- `DEPLOY_ATG` - indicates whether the build will be used for atg pages also or not
- `DISABLE_TYPE_CHECKING` - disables type checking in development
- `DISABLE_WEBPACK_PROGRESS` - disables webpack progress indicator in CLI
- `ENABLE_HTTP_LOGGING` - logs all http requests being made
- `GATSBY_BASE_PATH` - site to build against (no trailing slash)
- `GATSBY_ENV` - indicates whether the application is running in a production environment or not
- `GATSBY_GOOGLE_API_KEY` - api key for google maps
- `GATSBY_MAGNOLIA` - indicates whether the application is running in magnolia or not
- `GATSBY_MAGNOLIA_URL` - magnolia rest api url (including .rest)
- `GATSBY_MAGNOLIA_ENDPOINT_CONTENT` - magnolia pages rest endpoint for content (relative from .rest)
- `GATSBY_MAGNOLIA_ENDPOINT_CONTENT_TEMPLATES` - pipe separated list of templates to optionally filter content endpoint by, e.g. 'attract:pages/StoreDetails|attract:pages/StoreLocation'
- `GATSBY_MAGNOLIA_ENDPOINT_HEALTH_HUB_CATEGORIES` - magnolia categories rest endpoint (relative from .rest)
- `GATSBY_MAGNOLIA_ENDPOINT_PAGES` - magnolia pages rest endpoint for locales (relative from .rest)
- `GATSBY_MAGNOLIA_ENDPOINT_PRODUCT_FLAGS` - magnolia product flags rest endpoint (relative from .rest)
- `GATSBY_MAGNOLIA_ENDPOINT_SITES` - magnolia pages rest endpoint for site pages (relative from .rest)
- `GATSBY_MAGNOLIA_REQUIRED_NODES` - comma separated list of names used by gatsby-source-magnolia to determine what needs fetching from magnolia
- `GATSBY_MAGNOLIA_LOCALE` - specifies the locale data to be returned from a single endpoint, i.e. 'en-ie' when hitting UK Health Hub
- `GATSBY_MAGNOLIA_CATEGORY_BASE_PATH` - specifies the base path for the categories if different from GATSBY_BASE_PATH
- `GATSBY_ONESEARCH_URL` - onesearch api url
- `GATSBY_QUEUE_URL` - basket/sitewide queue api url
- `GATSBY_SHARED` - indicates whether the application is running outside Gatsby or not
- `GATSBY_WOOSMAP_PUB_KEY` - api key for woosmap
- `GATSBY_ENABLE_SERVICE_WORKER` - enable service worker for build
- `ONESEARCH_URL` - onesearch api url (for build process)
- `ORIGIN_URL` - atg url the application will proxy in development
- `POLLY_MODE` - mode PollyJS will use (https://netflix.github.io/pollyjs/#/configuration?id=mode)
- `PROPERTY_URL` - property api url
- `RECORD_MISSING` - indicates whether PollyJS should record missing requests or not
- `STORYSHOTS` - indicates whether storybook is building for storyshots testing or not
- `VARIATIONS_URL` - sitespect variations url the application will proxy in development
- `REQUIRED_HTML_PAGES` - if those pages are missing on the Gatsby build, the cronjob will stop deploying any content
- `SITESPECT_API_KEY` - sitespect API key (should use separate key per build)
- `GATSBY_RFL_CARD_API` - rlf card api endpoint

## Creating Sitemaps

Sitemap creation runs on a seperate pipeline to the rest of our builds. In order to create sitemaps locally run:

`GATSBY_MAGNOLIA_URL=https://magnolia-public.eu-west-1.prod.hbi.systems/.rest HREFLANG_ENABLED='true' yarn create-sitemaps`

You can use the debugger tool with this by temporarily adding the env variables to the `create-sitemaps` script in the package.json, and in the debugger tool selecting `Node.js (preview)` from the dropdown and selecting the `create-sitemaps` script.

## Vertical Rhythm

In order to ensure consistant [vertical rhythm](https://zellwk.com/blog/why-vertical-rhythms/) between elements across our site, we should adhere to [UX's design guidelines](https://www.figma.com/file/QwmJbHMdC2SEmN7z4nVcXK/Spacing-%26-Grids?node-id=0%3A1) on vertical spacing.

#### Usage

In order to apply vertical rhythm, add the appropriate [vertical rhythm CSS variable](https://gitlab.com/HnBI/replatform/hbi-content/-/blob/develop/src/styles/variables.css) (which follow [UX's rule of 8 spacing guidelines](https://www.figma.com/file/QwmJbHMdC2SEmN7z4nVcXK/Spacing-%26-Grids?node-id=1%3A707)) to the `margin-bottom` value for your component in your CSS module.

`.banner { margin-bottom: var(--vertical-spacing-2); }`

When in doubt about what spacing to use between components, consult either the [UX's design guidelines](https://www.figma.com/file/QwmJbHMdC2SEmN7z4nVcXK/Spacing-%26-Grids?node-id=0%3A1) or a UX team member. For our dynamically programmed components, we are using the [Grid component wrapper](https://gitlab.com/HnBI/replatform/hbi-content/-/blob/develop/src/components/Grid/Grid.module.css) to automatically apply margins between child components (which also allows for exceptions where necessary).

## Lighthouse CI Server

Track our historical page speed scores with the below links.

- [Feature and develop branches](https://lighthouse-ci-server-internal-eks.eu-west-1.dev.hbi.systems/app/projects/lighthouse-ci-server/dashboard)
- [Master branch](https://lighthouse-ci-server-internal-eks.eu-west-1.prod.hbi.systems/app/projects/lighthouse-ci-server/dashboard)

## Generate new components

To automatically generate templated files for a new component run `yarn generate [INSERT COMPONENT NAME]`. If you would like to create additional magnolia files run `yarn generate [YOUR COMPONENT NAME] [YOUR MAGNOLIA AREA]` - the magnolia area should be either: `areas`, `components` or `pages`.

### Further Documentation

- [Shared Components](./shared/README.md)
- [Storybook](./.storybook/README.md)
- [UX Experiments](./src/experiments/README.md)
- [Magnolia groovy workflow commands](./magnolia/scripts/groovy/workflow/commands/README.md)

### Notes

- Site created with [GatsbyJS](https://github.com/gatsbyjs/gatsby).
- [Useful links](https://hbidigital.atlassian.net/wiki/spaces/CONT/pages/1434877999/Attract+squad+useful+links)
- [Deployments](https://hbidigital.atlassian.net/wiki/spaces/CONT/pages/1460764927/Magnolia+deployments+cronjob)
