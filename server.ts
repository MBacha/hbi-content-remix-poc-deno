import { serve } from 'https://deno.land/std@0.153.0/http/server.ts';
import { createRequestHandlerWithStaticFiles } from '@remix-run/deno';
import * as build from '@remix-run/dev/server-build';

const remixHandler = createRequestHandlerWithStaticFiles({
  build,
  mode: Deno.env.get('NODE_ENV'),
  getLoadContext: () => ({}),
});

serve(async request => {
  console.time(request.url);
  const response = await remixHandler(request);
  console.timeEnd(request.url);

  return response;
});
