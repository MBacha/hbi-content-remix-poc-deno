/** @type {import('@remix-run/dev').AppConfig} */
module.exports = {
  serverBuildTarget: 'deno',
  server: './server.ts',
  devServerBroadcastDelay: 300,
  ignoredRouteFiles: ['**/.*'],
};
