import { ScriptRegistry, ScriptRegistryProvider } from '@hnbi/hbi-content';
import { RemixServer } from '@remix-run/react';
import type { EntryContext } from '@remix-run/server-runtime';
import { renderToString } from 'react-dom/server';

export default async function handleRequest(
  request: Request,
  responseStatusCode: number,
  responseHeaders: Headers,
  remixContext: EntryContext
) {
  const scriptRegistry = new ScriptRegistry();

  console.time(`ssr for ${request.url}`);
  let body = renderToString(
    <ScriptRegistryProvider registry={scriptRegistry}>
      <RemixServer context={remixContext} url={request.url} />
    </ScriptRegistryProvider>
  );
  console.timeEnd(`ssr for ${request.url}`);

  body = body.replace('__SCRIPTS__', scriptRegistry.getScriptTags().join(''));

  responseHeaders.set('Cache-Control', 'no-store');
  responseHeaders.set('Content-Type', 'text/html');

  return new Response(body, {
    status: responseStatusCode,
    headers: responseHeaders,
  });
}
