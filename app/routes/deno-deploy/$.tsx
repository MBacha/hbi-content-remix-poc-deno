import { AppProvider, magnoliaConfig, PageProvider } from '@hnbi/hbi-content';
import { EditablePage } from '@magnolia/react-editor';
import { useLoaderData } from '@remix-run/react';
import type { LinksFunction, LoaderFunction } from '@remix-run/server-runtime';
import contentStyles from '../../../.yalc/@hnbi/hbi-content/shared/dist/main.css';
import styles from '../../styles/global.css';

export const links: LinksFunction = () => [
  { rel: 'stylesheet', href: styles },
  { rel: 'stylesheet', href: contentStyles },
];

export const loader: LoaderFunction = async ({ params }) => {
  const pagePath = params['*'] ? `/${params['*']}` : '/';
  console.time(`run loader for ${pagePath}`);

  try {
    const res = await fetch(
      `https://preprod-ie.hollandandbarrett.net/content-aggregator-poc/page?gatsbyImageCompat=true&basePath=/site/uk&pagePath=${pagePath}`
    );

    if (res.status === 404) {
      throw new Response(`page ${pagePath} not found`, { status: 404 });
    }

    return await res.json();
  } finally {
    console.timeEnd(`run loader for ${pagePath}`);
  }
};

export default function Page() {
  const { node, path, site, images } = useLoaderData();

  return (
    <PageProvider
      page={{
        pageContext: {
          localePath: { baseSubsitePath: '/deno-deploy', pagePath: path },
          pageNode: node,
          site,
          images,
        },
      }}
    >
      <AppProvider>
        <EditablePage config={magnoliaConfig} content={node} />
      </AppProvider>
    </PageProvider>
  );
}
